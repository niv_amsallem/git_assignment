#include "vector.h"
#include <iostream>

using namespace std;

Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	_resizeFactor = n;
	_capacity = n;
	_size = 0;
	this->_elements = new int[this->_resizeFactor];
}

Vector::~Vector()
{
	if (this->_elements)
	{
		delete[] this->_elements;
		this->_elements = NULL;
		this->_size = 0;
		this->_capacity = 0;
	}
}

int Vector::capacity() const
{
	return this->_capacity;
}

int Vector::size() const
{
	return this->_size;
}

int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

bool Vector::empty() const
{
	if (_size == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Vector::push_back(const int& val)
{
	if (this->_size < this->_capacity)
	{
		this->reserve(this->_resizeFactor + this->_capacity);
	}
	else
	{
		this->_size++;
		this->_elements[this->_size] = val;
	}
}

int Vector::pop_back()
{
	if (_size <= 0)
	{
		return -9999;
	}
	else
	{
		this->_size--;
		return _elements[_size - 1];
		
	}
}

void Vector::reserve(int n)
{
	if (n > _capacity)
	{
		int* temp = this->_elements;
		int calc = this->_capacity + this->_resizeFactor;
		while (n > calc)
		{
			calc += _resizeFactor;
		}
		this->_elements = new int[calc];
		this->_capacity = calc;
		for (int i = 0; i < this->_size; i++)
		{
			this->_elements[i] = temp[i];
		}
	}

}


void Vector::resize(int n)
{
	this->reserve(n);
	this->_size = n;
}

void Vector::assign(int val)
{
	for (int i = 0; i < _size; i++)
	{
		this->_elements[i] = val;
	}
}

void Vector::resize(int n, const int& val)
{
	int i;
	int from_here = _size;
	this->resize(n);
	for (i = from_here; i < _size; ++i)
	{
		this->_elements[i] = val;
	}
}

Vector::Vector(const Vector& other) : _elements(NULL)
{
	_size = other.size();
	_capacity = other.capacity();
	_resizeFactor = other.resizeFactor();

	this->_elements = new int[other.capacity()];
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}
}

int& Vector :: operator[](int n) const
{
	if (n >= this->_size || n < 0)
	{
		cout << "index out of range" << endl;
		n = 0;
	}
	return this->_elements[n];
}



Vector& Vector :: operator=(const Vector& other)
{
	delete[] _elements;
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this;
}